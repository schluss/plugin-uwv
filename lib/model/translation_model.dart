import 'package:flutter/cupertino.dart';
import 'package:uwv/util/translation_template.dart';

/*
 * @author Asanka Anthony
 * @email aanthony@yukon.lk
 * @create date 2020-11-13 20:30:23
 * @modify date 2020-11-13 20:30:23
 */

class TranslationModel {
  static final nl = 'NL';
  static final en = 'EN';
  static final uk = 'UK';
  
  String? backButtonText;
  String? loadingText;
  String? processingText;
  TranslationModel({
    this.backButtonText,
    this.loadingText,
    this.processingText,
  });

  static TranslationModel? getTranslation(Locale? locale) {
    Map<String, TranslationModel> translationList;
    translationList = TranslationTemplate.translationCollection();
    
    if (locale != null) {
      return translationList[locale.languageCode.toUpperCase()];
    }
    return translationList[nl];
  }
}
