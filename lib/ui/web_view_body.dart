import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:logger/logger.dart';
import 'package:pdf_text/pdf_text.dart';
import 'package:uwv/model/translation_model.dart';
import 'package:uwv/reused_widgets/wait_overlay.dart';
import 'package:uwv/ui/auth_sms_warning.dart';
import 'package:uwv/util/util.dart';

import '../reused_widgets/wait_overlay.dart';

Logger _logger = Logger();
Locale? _local;
late BuildContext ctx;
bool isShown = false;
late bool _isTestEnvironment;
Map<String, dynamic> attributes = <String, dynamic>{};

class WebViewBody extends StatefulWidget {
  final Function webViewCreatedCallback; // use to call after webview created
  final String initialUrl; // initial url to open
  final Function urlChangedCallback; // use to call after webview created
  final Function mainAppCallBack; // main application callback function
  final Locale locale;
  final String pluginName;

  WebViewBody(
    this.webViewCreatedCallback,
    this.initialUrl,
    this.urlChangedCallback,
    this.mainAppCallBack,
    this.locale, {
    Key? key,
    this.pluginName = 'UWV',
  }) {
    _local = locale;

    // to keep track of whether we run this plugin on the test or real UWV environment
    _isTestEnvironment = initialUrl.contains('services.schluss.app');
  }

  @override
  State<WebViewBody> createState() => _WebViewBodyState();
}

class _WebViewBodyState extends State<WebViewBody> {
  @override
  void initState() {
    super.initState();
  }

  void showWaiter(String? text) async {
    if (!isShown) {
      WaitOverlay.show(ctx, _local, text: text);
      isShown = true;
    }
  }

  void hideWaiter() {
    if (isShown) {
      WaitOverlay.hide();
      isShown = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    ctx = context;

    final _cookieManager = CookieManager.instance();
    // to make sure we do not end up in a loop during DigiD login
    var isFirst = true;

    @override
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Container(
      height: height * 0.9,
      width: width,
      child: Column(
        children: <Widget>[
          Expanded(
            child: InAppWebView(
                initialUrlRequest: URLRequest(url: Uri.parse(widget.initialUrl)),
                initialOptions: InAppWebViewGroupOptions(
                    android: AndroidInAppWebViewOptions(useHybridComposition: true),
                    crossPlatform: InAppWebViewOptions(
                        cacheEnabled: false, // Changes to true or false to enable or disable cache.
                        clearCache: true, // Clears all the webview's cache.
                        userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36')),
                onWebViewCreated: (InAppWebViewController appWebViewController) async {
                  // make sure attributes are empty
                  attributes = <String, dynamic>{};

                  showWaiter(TranslationModel.getTranslation(widget.locale)!.processingText);
                  widget.webViewCreatedCallback(appWebViewController); //call given function after webview created
                },
                onLoadStart: (controller, url) async {
                  showWaiter(TranslationModel.getTranslation(widget.locale)!.processingText);
                },
                onLoadStop: (InAppWebViewController controller, url) async {
                  var location = url.toString();
                  _logger.i('loaded url: ' + location);

                  // TEST ENVIRONMENT
                  if (_isTestEnvironment) {
                    // 1. login page
                    if (location == widget.initialUrl) {
                      hideWaiter();
                    }

                    // 2. fake DigiD SMS verification
                    if (location.contains('digid-sms.html')) {
                      hideWaiter();
                    }

                    // 2. loongegevens page
                    if (location.contains('loongegevens.html')) {
                      // download pdf
                      await parsePDF('https://services.schluss.app/uwv/test/downloads/verzekeringsbericht.pdf', _cookieManager);

                      // parse loongegevens page
                      await parseLoongegevensPage(controller);

                      // go to the next page
                      await controller.loadUrl(urlRequest: URLRequest(url: Uri.parse('https://services.schluss.app/uwv/test/arbeidsverleden.html')));
                    }

                    // 3. arbeidsverleden page
                    if (location.contains('arbeidsverleden.html')) {
                      // parse arbeidsverleden page
                      await parseArbeidsverledenPage(controller);
                      // redirect to logout page
                      await controller.loadUrl(urlRequest: URLRequest(url: Uri.parse('https://services.schluss.app/uwv/test/uitgelogd.html')));
                    }

                    // 4. After logout: we can assume all steps are done successfully, return to mainApp.
                    if (location.contains('uitgelogd.html')) {
                      await controller.clearCache();
                      hideWaiter();
                      // done, return
                      widget.mainAppCallBack(jsonEncode(attributes), context);
                    }
                  }
                  // LIVE ENVIRONMENT
                  else {
                    // 1. login / authentication

                    // after loading initialUrl, when redirected to the login landing page. Automatically click on the DigiD login link:
                    // yes, there are two click attempts, this is because sometimes an upcoming (maintenance) message is shown, then the html is little different, therefore the click needs to be done on another place
                    if (location.contains('https://www.uwv.nl/particulieren/mijnuwv/index.aspx?ReturnUrl')) {
                      await controller.evaluateJavascript(source: '''(function(){
                        var schlussLink = document.querySelector('#mainContainer > div > div:nth-child(2) > div > div:nth-child(2) > div > a');
                        if (schlussLink){
                          schlussLink.click();
                        }
                        var schlussLink2 = document.querySelector('#mainContainer > div > div:nth-child(3) > div > div:nth-child(2) > div > a');
                        if (schlussLink2){
                          schlussLink2.click();
                        }                        
                      })();''');
                    }

                    // when on the digid main login page
                    // we get here when:
                    // a: entering login landing page: redirect to login with username & password page (=inloggen_sms)
                    // b: post after login: do not do anything here (isfirst is already set, which prevents an endless loop)
                    // c: login failed because sms verification is disabled: show AuthSMSWarning
                    if (location == 'https://digid.nl/inloggen') {
                      // check if sms auth is enabled, if not: show error to the user
                      bool smsAuthDisabled = await (controller.evaluateJavascript(source: '''
                      (function(){
                        let el = document.querySelector("#main_content > div.block-with-icon--error > p");
                        if (el) {
                          console.log('innerText sms check is: ' + el.innerText);
                          return el.innerText.includes('sms');
                        }
                        return false;
                      })();'''));

                      if (smsAuthDisabled) {
                        hideWaiter();
                        Navigator.pop(context);
                        await Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => AuthSMSWarning(widget.locale)),
                        );
                        return;
                      }

                      if (isFirst) {
                        isFirst = false;
                        await controller.loadUrl(urlRequest: URLRequest(url: Uri.parse('https://digid.nl/inloggen_sms')));
                      }
                    }

                    // when at digid login / auth pages: hide waiter to allow user
                    if (location == 'https://digid.nl/inloggen_sms' || location == 'https://digid.nl/sms_controleren' || location == 'https://digid.nl/bevestig_telefoonnummer') {
                      hideWaiter();
                    }

                    // 2. loongegevens page
                    if (location == 'https://mijn.uwv.nl/verzekeringsbericht/Default.aspx?ac=BekijkSVSpecificatie.ascx') {
                      // download pdf
                      await parsePDF('https://mijn.uwv.nl/verzekeringsbericht/Default.aspx?ac=PdfVersieVerzekeringsBericht.ascx&burgerservicenummer=false&arbeidsverleden=true&arbeidsverledenDetailgegevens=true&loongegevens=true', _cookieManager);

                      // parse loongegevens page
                      await parseLoongegevensPage(controller);

                      // go to the next page
                      await controller.loadUrl(urlRequest: URLRequest(url: Uri.parse('https://mijn.uwv.nl/verzekeringsbericht/Default.aspx?ac=BekijkArbeidsVerledenPagina.ascx')));
                    }

                    // 3. arbeidsverleden page
                    if (location == 'https://mijn.uwv.nl/verzekeringsbericht/Default.aspx?ac=BekijkArbeidsVerledenPagina.ascx') {
                      // parse arbeidsverleden page
                      await parseArbeidsverledenPage(controller);

                      // redirect to logout page
                      await controller.loadUrl(urlRequest: URLRequest(url: Uri.parse('https://mijn.uwv.nl/mijnuwv/logout')));
                    }

                    // 4. After logout: we can assume all steps are done successfully, return to mainApp.
                    if (location == 'https://www.uwv.nl/particulieren/mijnuwv/uitgelogd.aspx') {
                      await controller.clearCache();
                      hideWaiter();

                      // done, return
                      widget.mainAppCallBack(jsonEncode(attributes), context);
                    }
                  }
                }),
          )
        ],
      ),
    );
  }

  void closeOrBack() {
    hideWaiter(); // hide waiting screen
    Navigator.pop(ctx); // call previous UI
  }
}

Future parseLoongegevensPage(InAppWebViewController controller) async {
  // parse loongegevens
  var loonGegevens = await controller.evaluateJavascript(
    source: await DefaultAssetBundle.of(ctx).loadString('packages/uwv/assets/js/loongegevensParser.js'),
  );

  var tmpMap = Map<String, dynamic>.from(loonGegevens); // Explicit Cast to <String, dynamic>, otherwise IOS error

  // add to attributes global
  attributes.addAll(tmpMap);
}

Future parseArbeidsverledenPage(InAppWebViewController controller) async {
  // parse arbeidsVerleden
  var arbeidsVerleden = await controller.evaluateJavascript(
    source: await DefaultAssetBundle.of(ctx).loadString('packages/uwv/assets/js/arbeidsverledenParser.js'),
  );

  var tmpMap = Map<String, dynamic>.from(arbeidsVerleden); // Explicit Cast to <String, dynamic>, otherwise IOS error

  // add to attributes global
  attributes.addAll(tmpMap);
}

/*
 * download file & save 
 */
Future<void> parsePDF(String url, CookieManager _cookieManager) async {
  List<Cookie?> cookies;
  cookies = await _cookieManager.getCookies(url: Uri.parse(url));

  // IOS specific fix:
  // These 2 session cookies do not show up when calling getCookies. Retrieve them manually. If not: session not available and download fails.
  var ewSessionCookie = await _cookieManager.getCookie(url: Uri.parse('https://uwv.nl'), name: 'EWSESSION');

  if (ewSessionCookie != null) {
    cookies.add(ewSessionCookie);
  }

  var ewSessionSCookie = await _cookieManager.getCookie(url: Uri.parse('https://uwv.nl'), name: 'EWSESSION_S');

  if (ewSessionSCookie != null) {
    cookies.add(ewSessionSCookie);
  }

  _logger.i('UWV plugin - download file: ' + url);
  String savePath;
  savePath = await Util.downloadAndSaveFile(url, cookies);

  // store file itself (as base64 encoded bytes)
  File file;
  file = File(savePath); // get file content to object
  List<int> pdfBytes = await file.readAsBytes();
  attributes['InsuranceMessage'] = Base64Codec().encode(pdfBytes);

  // get gender / initials from pdf
  PDFDoc doc;
  String docText;
  doc = await PDFDoc.fromPath(savePath);
  docText = await doc.text;

  attributes['Gender'] = getGender(docText);

  // finally, delete the stored file
  await file.delete(recursive: true);
}

String getGender(String doc) {
  if (doc.contains('Mevrouw')) {
    return 'Female';
  } else if (doc.contains('De heer')) {
    return 'Male';
  } else {
    return 'Unknown';
  }
}
