/*
 * @author Asanka Anthony
 * @email aanthony@yukon.lk
 * @create date 2020-11-12 09:42:00
 * @modify date 2020-11-12 09:42:00
 * @desc Loading screen until web view loaded
 */

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:uwv/constants/ui_constants.dart';
import 'package:uwv/model/translation_model.dart';
import 'package:uwv/reused_widgets/auth_warning_list.dart';
import 'package:uwv/reused_widgets/backward_button.dart';
import 'package:uwv/reused_widgets/closing_button.dart';
import 'package:uwv/reused_widgets/main_button.dart';

late BuildContext ctx;

class AuthSMSWarning extends StatelessWidget {
  final Locale locale;

  AuthSMSWarning(this.locale);

  @override
  Widget build(BuildContext context) {
    ctx = context;
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    TranslationModel.getTranslation(locale)!.loadingText;

    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).padding.top + height * 0.1,
            ),
            height: height,
            width: width,
            color: UIConstants.primaryColor,
            child: Container(
              height: height * 0.9,
              width: width,
              child: Padding(
                  padding: EdgeInsets.only(
                    left: width * 0.07,
                    right: width * 0.07,
                    bottom: 0,
                  ),
                  child: ListView(children: [
                    SizedBox(height: 20),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/images/digid_and_sms.png',
                          package: 'uwv',
                          width: width * 0.4,
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    Text(
                      'Oeps! Je sms-controle staat uit',
                      style: TextStyle(
                        color: UIConstants.headerTitleText,
                        fontSize: width * 0.05,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 20),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'Dat heb je nodig voor inloggen met DigiD bij UWV.',
                          style: TextStyle(
                            color: UIConstants.mediumGrey,
                            fontSize: width * 0.05,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    AuhtWarningList([
                      AuhtWarningListItem(
                        '1',
                        'Ga naar',
                        child: RichText(
                          text: TextSpan(
                            text: '',
                            style: TextStyle(
                              color: UIConstants.mediumGrey,
                              fontSize: width * 0.05,
                            ),
                            children: [
                              TextSpan(
                                text: 'www.digid.nl',
                                style: TextStyle(
                                  decoration: TextDecoration.underline,
                                ),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () async {
                                    const url = 'https://www.digid.nl';
                                    if (await canLaunch(url)) await launch(url, forceWebView: false, forceSafariVC: false);
                                  },
                              ),
                            ],
                          ),
                        ),
                      ),
                      AuhtWarningListItem('2', 'Klik op Mijn DigiD'),
                      AuhtWarningListItem('3', 'Log in'),
                      AuhtWarningListItem('4', 'Ga naar \'sms-controle\' en klik op \'sms-controle activeren\''),
                    ]),
                    SizedBox(height: 20),
                    Padding(
                      padding: EdgeInsets.only(
                        right: width * 0.075,
                        left: width * 0.075,
                        bottom: 0,
                      ),
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: MainButton('Opnieuw inloggen bij UWV', triggerBack),
                      ),
                    )
                  ])),
            ),
          ),
          // header
          Stack(
            children: <Widget>[
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    height: height * 0.1,
                    color: UIConstants.paleLilac,
                  )),
              Padding(
                  padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(20),
                        topRight: const Radius.circular(20),
                      ),
                    ),
                    height: height * 0.1,
                    alignment: Alignment.center,
                    child: Stack(
                      children: <Widget>[
                        // back arrow
                        Padding(
                          padding: EdgeInsets.only(left: height * 0.02),
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: BackwardButton(
                              locale: locale,
                              call: triggerBack,
                            ),
                          ),
                        ),
                        // circle close button
                        Padding(
                          padding: EdgeInsets.only(right: height * 0.02),
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: ClosingButton(
                              call: triggerBack,
                            ),
                          ),
                        ),
                      ],
                    ),
                  )),
            ],
          ),
        ],
      ),
    );
  }

  /*back button trigger */
  void triggerBack() {
    Navigator.pop(ctx);
  }
}
