import 'package:flutter/material.dart';
import 'package:uwv/constants/ui_constants.dart';
import 'package:uwv/model/translation_model.dart';

class BackwardButton extends StatelessWidget {
  final Locale? locale;
  final Color color;
  final Function? call;

  BackwardButton({
    this.locale,
    this.color = UIConstants.mediumGrey,
    this.call,
  });

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Align(
      alignment: Alignment.centerLeft,
      child: GestureDetector(
        onTap: () => call ?? Navigator.pop(context),
        child: Row(
          children: <Widget>[
            Icon(
              Icons.keyboard_arrow_left,
              color: color,
              size: width * 0.06,
            ),
            Text(
              TranslationModel.getTranslation(locale)!.backButtonText!,
              style: TextStyle(
                color: color,
                fontFamily: 'Graphik',
                fontSize: width * 0.042,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
