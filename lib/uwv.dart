library uwv;

import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uwv/interface/common_plugin_dao.dart';
import 'package:uwv/ui/web_view_loading.dart';

class PluginUWV implements CommonPluginDAO {
  static String? test2;
  final StreamController<int> _streamController = StreamController.broadcast();
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  @override
  void runPlugin(
    context,
    Function callBack,
    String siteUrl,
    String trigger, {
    String pluginName = 'UWV',
    Function(String userName, String password)? storeCredentialCallback,
    String? userName,
    String? password,
  }) {
    Locale locale;
    locale = Localizations.localeOf(context);

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => WebViewLoading(
          callBack,
          siteUrl,
          trigger,
          locale,
          pluginName: pluginName,
          storeCredentialCallback: storeCredentialCallback,
          userName: userName,
          password: password,
        ),
      ),
    );
  }

//Extract data from xml file
  @override
  Future<Map<String, dynamic>> getExtractedData(String jsonData) async {
    _streamController.add(40);
    Map<String, dynamic>? attributes = jsonDecode(jsonData);
    _streamController.add(100);
    return Future.value(attributes);
  }

  Stream<int> getProgress() {
    return _streamController.stream; // return stream controller to the main app
  }
}
