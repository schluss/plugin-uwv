(function () {
    let r = {};

    // Name
    let name = getAttribute('#header > div:nth-child(3) > div > strong');
    let split = name.lastIndexOf('.');
    r.Initials = name.substring(0, split + 1).trim();
    r.Surname = name.substring(split + 1).trim();

    // Date of birth
    r.DateOfBirth = formatDate(getAttribute('#header > div:nth-child(3) > div > span.userstatus_details > span.userstatus_birthdate'));

    // Incomes[]
    var sections = document.querySelectorAll('section');

    var loonItems = [];
    sections.forEach((section) => {
        if (section.id != '') {
            var item = {};

            item.Employer = getAttribute('#' + section.id + ' > div > dl > dd:nth-child(2)');
            item.PayrollTaxNumber = getAttribute('#' + section.id + ' > div > dl > dd:nth-child(4)');
            item.Type = getAttribute('#' + section.id + ' > div > dl > dd:nth-child(8)');
            item.IncomeDetails = [];

            //InsuredLaws
            insuredLaws = getAttribute('#' + section.id + ' > div > dl > dd:nth-child(6)');
            insuredLaws = insuredLaws.replace('en', ',');
            insuredLaws = insuredLaws.replace(/ /g, '');
            item.InsuredLaws = insuredLaws.split(',');

            // svLoon details list
            var details = document.querySelectorAll('#' + section.id + ' > div > table > tbody > tr');
            details.forEach((row) => {

                let Period = getAttribute('td:nth-child(1)', null, row);
                let periodSplit = Period.indexOf('t/m');
                let StartDate = Period.substring(0, periodSplit).trim();
                let EndDate = Period.substring(periodSplit + 3).trim();

                item.IncomeDetails.push({
                    //'Period': Period,
                    'StartDate': formatDate(StartDate),
                    'EndDate': formatDate(EndDate),
                    'Hours': getAttribute('td:nth-child(2)', null, row),
                    'Value': getAttribute('td:nth-child(3)', null, row).replace('€','').replace('.','').trim(),
                    'CarContribution': getAttribute('td:nth-child(4)', null, row).replace('€','').replace('.','').trim(),
                    'PrivateUseCar': getAttribute('td:nth-child(5)', null, row).replace('€','').replace('.','').trim()
                });
            });

            loonItems.push(item);
        }
    });

    r.Incomes = JSON.stringify(loonItems); // value as a json encoded string!

    return r;
})();

function formatDate(dateStr){
	if (!dateStr || dateStr.trim() == '' || !dateStr.includes('-'))
		return '';
	
	let dobArr = dateStr.trim().split('-');
	let dt = new Date(dobArr[2], (dobArr[1]-1), dobArr[0]);
	const offset = dt.getTimezoneOffset();
	dt = new Date(dt.getTime() - (offset*60*1000));
	return dt.toISOString().split('T')[0];
}

function getAttribute(selector, attribute, dom) {
    if (attribute == null) attribute = 'innerText';
    if (dom == null) dom = document;
    let el = dom.querySelector(selector);
    if (el == null) return '';

    return dom.querySelector(selector)[attribute].trim();
}
